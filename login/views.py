from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.db import connection

response ={}

def index(request):
    html ='login/landing.html'
    return render(request, html)

def loginpage(request):
    html = 'login/login.html'
    return render(request,html)

def login_as(request):
    lst = skema_beassiwa_aktif()
    response['list_beasiswa'] = lst
    print(response['list_beasiswa'])
    html='login/login_as.html'
    return render(request,html,response)

def login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        #Person.objects.raw('SELECT * FROM myapp_person WHERE last_name = %s', [lname])
        cr = connection.cursor()
        cr.execute("SELECT * FROM register_pengguna WHERE username= %s", [username])
        data = dictfetchall(cr)[0]

        user= data['username']
        pwd= data['password']
        role=data['role']



        print(data)
        print(user,pwd)
        if(username==user and password==pwd):

            request.session['username'] = username
            request.session['password'] = password
            request.session['role'] = role
            return HttpResponseRedirect(reverse('login:login_as'))
        else:
            return HttpResponseRedirect(reverse('register:index'))
        #else:
        #print('salah')

def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def logout (request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login:index'))

def skema_beassiwa_aktif ():
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM skema_beasiswa_aktif_skemabeasiswaaktif join skema_beasiswa_aktif_skemabeasiswa ON kode_skema_beasiswa_id = kode_skema_beasiswa')
    return dictfetchall(cursor)
