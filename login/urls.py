from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^userlogin/', loginpage, name='loginpage'),
    url(r'^login/', login, name='login'),
    url(r'^loginsuccess/', login_as, name='login_as'),
    url(r'^logout/', logout, name='logout'),
]
