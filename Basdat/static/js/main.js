(function($) {
	"use strict"

	///////////////////////////
	// Preloader
	$(window).on('load', function() {
		$("#preloader").delay(600).fadeOut();
	});


	///////////////////////////
	// Scrollspy
	$('body').scrollspy({
		target: '#nav',
		offset: $(window).height() / 2
	});

	///////////////////////////
	// Smooth scroll
	$("#nav .main-nav a[href^='#']").on('click', function(e) {
		e.preventDefault();
		var hash = this.hash;
		$('html, body').animate({
			scrollTop: $(this.hash).offset().top
		}, 600);
	});

	$('#back-to-top').on('click', function(){
		$('body,html').animate({
			scrollTop: 0
		}, 600);
	});

	///////////////////////////
	// Btn nav collapse
	$('#nav .nav-collapse').on('click', function() {
		$('#nav').toggleClass('open');
	});

	///////////////////////////
	// Mobile dropdown
	$('.has-dropdown a').on('click', function() {
		$(this).parent().toggleClass('open-drop');
	});

	///////////////////////////
	// On Scroll
	$(window).on('scroll', function() {
		var wScroll = $(this).scrollTop();

		// Fixed nav
		wScroll > 1 ? $('#nav').addClass('fixed-nav') : $('#nav').removeClass('fixed-nav');

		// Back To Top Appear
		wScroll > 700 ? $('#back-to-top').fadeIn() : $('#back-to-top').fadeOut();
	});

	///////////////////////////
	// magnificPopup
	$('.work').magnificPopup({
		delegate: '.lightbox',
		type: 'image'
	});





})(jQuery);

var listBeasiswa = [
	['Beasiswa 1','23/07/2018','Dibuka','200'],
	['Beasiswa 2','23/07/2018','Dibuka','200'],
	['Beasiswa 3','23/07/2018','Dibuka','200'],
	['Beasiswa 4','23/07/2018','Dibuka','200'],
	['Beasiswa 5','23/07/2018','Dibuka','200'],
]


listBeasiswa.map( (item) => {
	$('#products').append(
		'<div class="item list-group-item col-xs-4 col-lg-">' +
		'<div class="thumbnail">'+
		'<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />'+
		'<div class="caption">'+
		'<h4 class="group inner list-group-item-heading">' + item[0] + '</h4>' +
		'<p class="group inner list-group-item-text">Waktu tutup		: ' + item[1]+ '</p>'+
		'<p class="group inner list-group-item-text">Status	: ' + item[2] + '</p>'+
		'<p class="group inner list-group-item-text">Pendaftar	: ' + item[3] + '</p>'+
		'<a class="outline-btn" href="http://www.jquery2dotnet.com">Detail</a>'


	)

	$(".submit").on('click', ()=> {

	  var val = $('#select').val();
	  if(val=='.mhs'){
	    $(val).css('display', 'flex');
	    $(val).css('flex-direction', 'column');
	    $('.yayasan').css('display', 'none');
	    $('.individu').css('display','none');
	  }
	  else if(val=='.yayasan'){
	    $(val).css('display', 'flex');
	    $(val).css('flex-direction', 'column');
	    $('.individu').css('display','none');
	    $('.mhs').css('display','none');
	  }
	  else if(val=='.individu'){
	    $(val).css('display', 'flex');
	    $(val).css('flex-direction', 'column');
	    $('.mhs').css('display','none');
	    $('.yayasan').css('display','none');
	  }


	});
	var list = [
	['Beasiswa 1','23/07/2018','Dibuka','200'],
	['Beasiswa 2','23/07/2018','Dibuka','200'],
	['Beasiswa 3','23/07/2018','Dibuka','200'],
	['Beasiswa 4','23/07/2018','Dibuka','200'],
	['Beasiswa 5','23/07/2018','Dibuka','200'],
]

list.map( (item) => {
	$('.banner2').append(
    '<div class="list">'+
      '<img src="http://placehold.it/200x50/000/fff" alt="" />'+
      '<div class="item">' +
         '<h3>'+item[0]+'</h3>' +
          '<p>'+ 'Waktu tutup :'+item[1]+'</p>' +
          '<p>'+'Status :'+item[2]+'</p>' +
          '<p>'+'Pendaftar :'+item[3]+'</p>' +
    '<a class="btn" href="http://www.jquery2dotnet.com">Detail</a>'+
      '</div>'+
    '</div>'

	)
});
var tempat = [
['Beasiswa 1','Gedung A Fasilkom UI','09.00'],
['Beasiswa 2','Gedung B Fasilkom UI','10.00'],
['Beasiswa 3','Gedung C Fasilkom UI','08.00'],
['Beasiswa 4','Gedung D Fasilkom UI','07.00'],
['Beasiswa 5','Gedung E Fasilkom UI','06.00'],
]

tempat.map( (item) => {
$('.detailtempat').append(
	'<div class="list">'+
		'<img src="http://placehold.it/200x50/000/fff" alt="" />'+
		'<div class="item">' +
			 '<h3>'+item[0]+'</h3>' +
				'<p>'+ 'Lokasi:'+item[1]+'</p>' +
				'<p>'+'Waktu :'+item[2]+'</p>' +

		'</div>'+
	'</div>'

)
});



});
