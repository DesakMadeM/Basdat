from django.db import models

class Pengguna(models.Model):
    username = models.CharField(max_length = 20, primary_key = True)
    password = models.CharField(max_length = 20)
    role = models.CharField(max_length = 9)

class Donatur(models.Model):
    nomor_identitas = models.CharField(max_length = 20, primary_key = True)
    email = models.EmailField(max_length = 50)
    nama = models.CharField(max_length = 50)
    npwp = models.CharField(max_length = 20)
    no_telp = models.CharField (max_length = 20, blank = True)
    alamat = models.CharField (max_length = 50)
    username = models.ForeignKey(Pengguna, related_name = 'username_donatur', on_delete = models.CASCADE)

class Mahasiswa(models.Model):
    npm = models.CharField(max_length = 20, primary_key = True)
    email = models.EmailField(max_length = 50)
    nama = models.CharField(max_length = 50)
    no_telp = models.CharField(max_length = 20, blank = True)
    alamat_tinggal = models.CharField(max_length = 50)
    alamat_domisili = models.CharField(max_length = 50)
    nama_bank = models.CharField(max_length = 50)
    no_rekening = models.CharField(max_length = 20)
    nama_pemilik = models.CharField(max_length = 20)
    username = models.ForeignKey(Pengguna, related_name = 'username_mahasiswa', on_delete = models.CASCADE)

class IndividualDonatur(models.Model):
    nik = models.CharField(max_length = 16, primary_key = True)
    nomor_identitas_donatur = models.ForeignKey(Donatur, related_name = 'nomoridentitasdonaturindividual', on_delete = models.CASCADE)

class Yayasan (models.Model):
    no_sk_yayasan = models.CharField(max_length = 20, primary_key = True)
    email = models.EmailField(max_length = 50)
    nama = models.CharField (max_length = 50)
    no_telp_cp = models.CharField(max_length = 20, blank = True)
    nomor_identitas_donatur = models.ForeignKey(Donatur, related_name = 'nomoridentitasdonaturyayasan', on_delete = models.CASCADE)
